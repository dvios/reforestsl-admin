# Introduction

This is the admin panel of the Reforest Sri Lanka mobile application which is developed for IDEA Challenge 2016.

## Prerequisites
Node version >= 4.0 and NPM >= 3

## Following tools are required to run the project

* webpack
`npm install --global webpack`

* webpack-dev-server
`npm install --global webpack-dev-server`

* typescript
`npm install --global typescript@2.0.0`

** These commands may require sudo permision on linux platforms **

## Install the project

`npm install`

## Run the project

`npm start`

Go to `http://localhost:3000` on your browser.
